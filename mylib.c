#include "mylib.h"

char* some_string = SOME_MACRO;
struct my_struct some_struct = { "Hello", 1234 };

const int
some_int ()
{
	return 123;
}

const int
my_struct_int (struct my_struct *s)
{
	return s->my_int;
}

const char*
my_struct_string (struct my_struct *s)
{
	return s->my_string;
}
