(require 'asdf)

(setf asdf:*central-registry*
      (uiop:subdirectories (uiop:merge-pathnames* (uiop:getcwd) #P"./lisp-systems")))

(push (uiop:getcwd) asdf:*central-registry*)
