#define SOME_MACRO "Hello, World!"

struct my_struct {
	char* my_string;
	int my_int;
};

extern char* some_string;
extern struct my_struct some_struct;
extern const int some_int ();
extern const int my_struct_int (struct my_struct*);
extern const char* my_struct_string (struct my_struct*);
