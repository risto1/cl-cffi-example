(defpackage cl-ffi-example
  (:use :cl))

(in-package :cl-ffi-example)


(cffi:load-foreign-library (ffi:find-foreign-library '("mylib") (uiop:getcwd)))

(cffi:defcfun ("some_int" some-int) :int)

#+nil(some-int) ; 123


(cffi:defcvar ("some_string" *some-string*) :string)

#+nil(progn *some-string*) ; "Hello, World!"


(cffi:defcstruct my-struct (my-string :string) (my-int :int))

(cffi:defcvar ("some_struct" *some-struct*) (:struct my-struct))

#+nil(progn *some-struct*)

#+nil(cffi:foreign-slot-offset '(:struct my-struct) 'my-int)

#+nil(cffi:with-foreign-object (ptr '(:struct my-struct))
          ;; Initialize the slots
          (setf (cffi:foreign-slot-value ptr '(:struct my-struct) 'my-int) 42
                (cffi:foreign-slot-value ptr '(:struct my-struct) 'my-string) "hello")
          ;; Return a list with the coordinates
          (cffi:with-foreign-slots ((my-int my-string) ptr (:struct my-struct))
            (list my-int my-string)))


(cffi:defcfun ("my_struct_int" my-struct-int) :int '(:pointer (:struct my-struct)))

#+nil(my-struct-int (cffi:get-var-pointer '*some-struct*)) ; 1234


(cffi:defcfun ("my_struct_string" my-struct-string) :string '(:pointer (:struct my-struct)))

#+nil(my-struct-string (cffi:get-var-pointer '*some-struct*)) ; "Hello"
