CC = gcc
LDFLAGS = -shared
SOURCE = mylib.c
OBJECT=$(SOURCE:.c=.o)
SOBJECT=$(SOURCE:.c=.so)
LIBS =
CFLAGS = -g -Wall -fPIC

all: $(OBJECT) $(SOBJECT)

$(OBJECT): $(SOURCE)
	$(CC) $(CFLAGS) -o $@ -c $<

$(SOBJECT): $(OBJECT)
	$(CC) -shared $(OBJECT) -o $(SOBJECT)

.PHONY: clean
clean:
	rm -rf $(OBJECT) $(SOBJECT)
