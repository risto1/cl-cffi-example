# cl-cffi-example

## Setup

Install the dependencies (submodules) into the `lisp-modules` folder:

```sh
git submodule init
git submodule update
```

Setup ASDF to locate the systems:

```lisp
(load "setup-asdf.lisp")
```

Load the system:

```lisp
(asdf:load-system :cl-ffi-example)
```
